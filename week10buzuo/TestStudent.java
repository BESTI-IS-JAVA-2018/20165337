import junit.framework.TestCase;

import java.util.LinkedList;
import java.util.List;
import java.util.*;

public class TestStudent extends TestCase {
    public static void main(String[] args) {
        List<Student> list = new LinkedList<Student>();
        list.add(new Student("20165335", "薛茂哲",570.0));
        list.add(new Student("20165336", "康志强",560.0));
        list.add(new Student("20165337", "岳源",520.0));
        list.add(new Student("20165338", "冶宇航",630.0));
        list.add(new Student("20165339", "唐羽瞳",650.0));
        Iterator<Student> iter = list.iterator();
        System.out.println("排序前,链表中的数据");
        Collections.shuffle(list);
        while (iter.hasNext()) {
            Student stu = iter.next();
            System.out.println(stu.getId() + " " + stu.getName()+ " "+stu.getTotalScore());
        }
        Collections.sort(list,new scoreComparator());
        System.out.println("排序后,链表中的数据");
        iter = list.iterator();
        while (iter.hasNext()) {
            Student stu = iter.next();
            System.out.println(stu.getId() + " " + stu.getName()+ " "+stu.getTotalScore());
        }
    }
}
