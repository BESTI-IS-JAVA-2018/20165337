import java.util.Comparator;
public class IDComparator implements Comparator {
    @Override
    public int compare(Object o1, Object o2) {
        Student st1 = (Student)o1;
        Student st2 = (Student)o2;
        return (Integer.parseInt(st1.getId())-Integer.parseInt(st2.getId()));
    }
}
