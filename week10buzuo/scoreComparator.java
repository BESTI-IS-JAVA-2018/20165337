import java.util.Comparator;
public class scoreComparator implements Comparator {
    @Override
    public int compare(Object o1, Object o2) {
        Student st1 = (Student)o1;
        Student st2 = (Student)o2;
        return (int) (st1.getTotalScore()-st2.getTotalScore());
    }
}
