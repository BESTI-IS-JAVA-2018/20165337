import java.util.*;
class Student implements Comparable {
    int english=0;
    String name;
    Student(int english,String name) {
        this.name=name;
        this.english=english;
    }
    public int compareTo(Object b) {
        Student st=(Student)b;
        return (this.english-st.english);
    }
}
public class Y {
    public static void main(String args[]) {
        List<Student> list=new LinkedList<Student>();
        int score []={66,77,88,99,55,87,61,52};
        String name[]={"yy","wy","dj","rip","lon","sq","lq","lx"};
        for(int i=0;i<score.length;i++){
            list.add(new Student(score[i],name[i]));
        }
        Iterator<Student> iter=list.iterator();
        TreeSet<Student> mytree=new TreeSet<Student>();
        while(iter.hasNext()){
            Student stu=iter.next();
            mytree.add(stu);
        }
        Iterator<Student> te=mytree.iterator();
        while(te.hasNext()) {
            Student stu=te.next();
            System.out.println(""+stu.name+" "+stu.english);
        }
    }
}

