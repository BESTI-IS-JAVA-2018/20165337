public class CPU {
    CPU (){

    }
    CPU (int m) {
        this.speed = m;
    }

    public void setSpeed(int m) {
        this.speed = m;
    }

    public int getSpeed() {

        return speed;
    }

    int speed;

    @Override
    public String toString() {
        return "CPU{" +
                "speed=" + speed +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CPU cpu = (CPU) o;
        return getSpeed() == cpu.getSpeed();
    }


}


