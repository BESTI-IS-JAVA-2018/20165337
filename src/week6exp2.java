public class exp2 {
    public static void main(String[] args) {
        digui D = new digui();
        int [] tmp = new int[args.length];
        for (int i = 0;i<args.length;i++){
            tmp[i] = Integer.parseInt(args[i]);
        }
        System.out.println("C("+tmp[0]+","+tmp[1]+") = "+D.C(tmp[0],tmp[1]));
    }
}

class digui {
    int C(int n,int m) {
        if ((m>=0)&&(m<=n)) {
            if ((m==0)||(m==n))
                return 1;
            else
                return C(n - 1, m - 1) + C(n - 1, m);
        }
        else {
            System.out.println("Error:require 0<=m<=n. "+"m = "+m+",n = "+n);
            System.exit(0);
        }
        return 0;
    }
}

