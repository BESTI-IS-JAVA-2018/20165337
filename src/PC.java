import java.util.Objects;

public class PC {
    PC(){

    }
    PC (CPU cpu) {
        this.cpu = cpu;
    }
    PC (HardDisk HD) {
        this.HD = HD;
    }
    PC (CPU cpu,HardDisk HD) {
        this.cpu = cpu;
        this.HD = HD;
    }

    public void setCpu(CPU c) {
        this.cpu = c;
    }

    public void setHardDisk(HardDisk h) {
        this.HD = h;
    }

    CPU cpu;
    HardDisk HD;

    void show() {
        System.out.println("The speed of CPU:"+cpu.speed);
        System.out.println("The amount of HD:"+HD.amount);
    }

    @Override
    public String toString() {
        return "PC{" +
                "cpu=" + cpu +
                ", HD=" + HD +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PC pc = (PC) o;
        return Objects.equals(cpu, pc.cpu) &&
                Objects.equals(HD, pc.HD);
    }

}
