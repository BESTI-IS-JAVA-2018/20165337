import junit.framework.TestCase;
import org.junit.Test;

public class FushuTest extends TestCase {
    Fushu.Complex a = new Fushu.Complex(5.0, 6.0);
    Fushu.Complex b = new Fushu.Complex(-3.0, 4.0);

    @Test
    public void testComplexAdd() {
        assertEquals(new Fushu.Complex(2.0, 10.0), a.ComplexAdd(b));
    }

    @Test
    public void testComplexSub() {
        assertEquals(new Fushu.Complex(8.0, 2.0), a.ComplexSub(b));
    }

    @Test
    public void testComplexMulti() {
        assertEquals(new Fushu.Complex(-39.0, 2.0), a.ComplexMulti(b));
    }
}
