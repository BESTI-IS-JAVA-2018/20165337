import junit.framework.TestCase;
import org.junit.Test;
import static java.lang.Math.sqrt;
import static java.lang.StrictMath.round;

public class Fushu {
    public static void main (String args[]){
        Complex wy = new Complex(1.0,4.0);
        Complex cc = new Complex(1.0,3.0);
    }
    static class Complex{
        double x;
        double y;
        Complex(double x,double y){
            this.x=x;
            this.y=y;
        }
        Complex ComplexAdd(Complex a){
            return new Complex(x+a.x,y+a.y );
        }
        Complex ComplexSub(Complex a){
            return new Complex(x-a.x,y-a.y );
        }
        Complex ComplexMulti(Complex a){
            return new Complex(x*a.x-y*a.y,x*a.y+y*a.x);
        }
        Complex ComplexDiv(Complex a){
            double g = sqrt(a.x*a.x)+sqrt(a.y*a.y);
            double s = (x*a.y-y*a.x);
            return new Complex ((x*a.x)+(y*a.y)/g,round(s/g));
        }
        @Override
        public boolean equals(Object o) {
            if (this == o) {
                return true;
            }
            if (o == null || getClass() != o.getClass()) {
                return false;
            }
            Complex complex = (Complex) o;
            return Double.compare(complex.x, x) == 0 &&
                    Double.compare(complex.y, y) == 0;
        }
        @Override
        public String toString() {

            if (y > 0) {
                return "("+x+"+"+y+"i"+")";
            }
            else
            {
                return "("+x+"-"+y+"i"+")";
            }
        }
    }
}

class TestFushu extends TestCase {
    Fushu.Complex a = new Fushu.Complex(5.0,6.0);
    Fushu.Complex b = new Fushu.Complex(-3.0,4.0);
    @Test
    public void testComplexAdd (){
        assertEquals(new Fushu.Complex(2.0,10.0),a.ComplexAdd(b));
    }
    @Test
    public void testComplexSub (){
        assertEquals(new Fushu.Complex(8.0,2.0),a.ComplexSub(b));
    }
    @Test
    public void testComplexMulti (){
        assertEquals(new Fushu.Complex(-39.0,2.0),a.ComplexMulti(b));
    }
}
